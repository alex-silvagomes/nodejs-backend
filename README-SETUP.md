1. o generate a self-signed certificate, run the following in your shell:

        openssl genrsa -out key.pem
        openssl req -new -key key.pem -out csr.pem
        openssl x509 -req -days 9999 -in csr.pem -signkey key.pem -out cert.pem
        rm csr.pem



a) kill processes
taskkill /F /IM node.exe


2. Build and Export PUSH DOCKER IMAGE to DockerHub

        docker build -t alexsilvagomes/nodejs-backend . 
        docker run -d -p 3000:3000 -p 443:443 -t alexsilvagomes/nodejs-backend
        docker images
        docker push alexsilvagomes/nodejs-backend:latest


3. Kind On Windows via Chocolatey (https://chocolatey.org/packages/kind)
        
        $ choco install kind

4. Criando Cluster
        
        $ kind create cluster --name clusterfullcycle
        $ kubectl cluster-info --context kind-clusterfullcycle


5. GCP Kubernetes Manager (Shell Console)

> Get Url do Cluster
        kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}' 


# https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html
> Integracao com gitlab

        vi gitlab-admin-service-account.yaml
        kubectl apply -f gitlab-admin-service-account.yaml


# 
> get Secrets
        kubectl get secrets        
        result: default-token-82qxt

        kubectl get secret default-token-82qxt -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

        kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')