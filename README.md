# README INMETRICS
## (api-quality-gate) Guia de configuracao do workspace de desenvolvimento
### 01. Configure o ambiente de desenvolvimento: 
api-quality-gate em NodeJS

1.1. Baixar e instalar o NodeJS 12+
>__download__: https://nodejs.org/en/download/

1.2. Faca um clone do repositorio do Bitbucket

> Execute comando: Terminal (CMD)
```
  git clone "Gitlab Inmetrics"
  cd nodejs-backend
  npm install
  npm start
```

1.3. Referencias e Bibliotecas
  * mssql: https://tediousjs.github.io/node-mssql/ 
    
### URIs Info
  * __healthcheck__: http://localhost:3000/api/health
  * __list-users__: http://localhost:3000/api/list-users


TODO:
https://imasters.com.br/back-end/api-http-rest-conceito-e-exemplo-em-node-js